<?php

namespace Train\CoreBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Train\CoreBundle\Entity\Voyage;
use Train\CoreBundle\Entity\Voyageur;
use Train\CoreBundle\Entity\Billet;
use Train\CoreBundle\Entity\Entier;
use Train\CoreBundle\Entity\Proposition;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller
{
	public function indexAction(Request $request){
		$doctrine = $this->getDoctrine();
		$em=$doctrine->getManager();
		$rep = $em->getRepository('TrainCoreBundle:Voyage');
		
		
		        return $this->render('TrainCoreBundle:Default:index.html.twig');
		
		
	}
	public function searchAction(Request $request){
		$doctrine = $this->getDoctrine();
		$em=$doctrine->getManager();
		$rep=$doctrine->getRepository('TrainCoreBundle:Voyage');
		
		//Dans le cas où la destination demandée n'existe pas
		$proposition = $request->get('proposition');
		$destination = $request->get('destination');
		$depart = $request->get('depart');
		if(isset($proposition)){
							
							$prop= new Proposition('nouveau trajet',$depart.'=>'.$destination);
							$em->persist($prop);
							$em->flush();
							$request->getSession()->getFlashBag()->add('notice', 
							'Votre suggestion a bien été prise en compte, merci.');
							return $this->redirectToRoute('train_core_homepage');
		}
		
		
		$dest = $request->query->get('destination');
		$dep = $request->query->get('depart');
		$voyages = $rep->findBy(array('destination'=>$dest,'depart'=>$dep), array('date'=>'desc'), 10,0);
		
		return $this->render('TrainCoreBundle:Default:search.html.twig',array('voyages'=>$voyages,'destination'=>$dest,
		'depart'=>$dep));
		
	}
    public function inscriptionAction(Request $request)
    {
		$logger = $this->get('logger');
		$doctrine = $this->getDoctrine();
		$em = $doctrine->getManager();
		$rep = $em->getRepository('TrainCoreBundle:Voyage');
		
		
		
		$idVoyage = $request->query->get('id');
		
		$voyage = $rep->find((int)$idVoyage);
		
		$voyageur = new Voyageur();

		   

		if($voyage==null){
			throw new NotFoundHttpException("Désolé mais ce voyage n'existe pas!");
		}
		else{
			$formBuilder = $this->get('form.factory')->createBuilder('form', $voyageur); 
			    $formBuilder
      ->add('nom',      'text')
      ->add('prenom',     'text')
      ->add('email',   'email')
      ->add('age',    'integer')
	   ->add('numeroTel',    'integer')
      ->add('enregistrer',      'submit');
	  
	      $form = $formBuilder->getForm();
		  
		  
		  $form->handleRequest($request);
		  
		  if($form->isValid()){
			  //Si le formulaire est valide on crée un billet pour le voyageur
			  //Qu'on ajoute au Voyage
					
					$voyageur->setStatut($voyageur->getAge());

					if($voyage->nbBillets()<$voyage::MAX_BILLETS){
						
						if(!$voyage->findVoyageur($voyageur->getEmail())){
							$em->persist($voyageur);
							$billet = new Billet($voyage,$voyageur);
							$em->persist($billet);
							$em->persist($voyage);
							
							  
							$em->flush();
							 $request->getSession()->getFlashBag()->add('notice', 
							 'Vous êtes enregistré. Il ne reste plus qu\' à attendre le mail de paiement une fois
							 que votre groupe sera complet.');
							 $request->getSession()->getFlashBag()->add('warning', 
							 'Regardez votre boîte de courrier indésirable si vous ne voyez rien arriver!');
							 $request->getSession()->getFlashBag()->add('notice', 
							 'N\'oubliez pas de réserver le retour!');							 
							 							 
							 
							 
							 $em->refresh($voyage);
							 //on envoie un mail pour le paiement si on est au complet
							 if($voyage->nbBillets()==$voyage::MAX_BILLETS){
								 $voyage->sendPayment();
					
							 }
							 
						}
						else{
							$request->getSession()->getFlashBag()->add('alert','Un voyageur avec cet email est déjà
						enregistré.');
						}

				  }
				  else{
					  $request->getSession()->getFlashBag()->add('alert', 'Le voyage est complet, désolé..');					  
				  }

				  
			  
		  }
		  
		  
		  
		  
		  
		  return $this->render('TrainCoreBundle:Default:inscription.html.twig', array('voyage' => $voyage,'form' => $form->createView()));
		}
		
        
    }

	
	public function adminAction(Request $request){			
				if($this->get('security.context')->isGranted('ROLE_ADMIN')){
					$doctrine=$this->getDoctrine();
					$em=$doctrine->getManager();
					$rep=$em->getRepository('TrainCoreBundle:Voyage');
					

					
					$idDel=$request->get('idToDelete');
					if(isset($idDel)){
						$em->remove($rep->find($idDel));
						$em->flush();
						
					}
					$voyages=$rep->findAll();					
					
					return $this->render('TrainCoreBundle:Default:admin.html.twig', array('voyages'=>$voyages));
				}
				    $authenticationUtils = $this->get('security.authentication_utils');
				return $this->render('TrainUserBundle:Default:login.html.twig', array(
				  'last_username' => $authenticationUtils->getLastUsername(),
				  'error'         => $authenticationUtils->getLastAuthenticationError(),
				));
		
				  

	}
	
	public function admin_viewAction(Request $request){
				if($this->get('security.context')->isGranted('ROLE_ADMIN')){
				$idVoyage=$request->query->get('id');
				$em=$this->getDoctrine()->getManager();
				$rep = $em->getRepository('TrainCoreBundle:Voyage');
				
				$idToDel=$request->get('idToDel');
				if(isset($idToDel)){
					$rep2=$em->getRepository('TrainCoreBundle:Voyageur');
					$rep3=$em->getRepository('TrainCoreBundle:Billet');
					$voyageurToDel=$rep3->find($idToDel);
					$em->remove($voyageurToDel);
					$em->flush();
				}
				$idToUpdate=$request->get('idToUpdate');
				if(isset($idToUpdate)){
					$rep3=$em->getRepository('TrainCoreBundle:Billet');
					$billetToUpdate=$rep3->find($idToUpdate);
					if($billetToUpdate->getPaye()==false){
						$billetToUpdate->setPaye(true);						
					}
					else{
						$billetToUpdate->setPaye(false);						
					}
					$em->flush();
					$em->refresh($billetToUpdate);

					
				}
				
				$voyage = $rep->find((int)$idVoyage);
				if(isset($voyage)){
					$billets= $voyage->getBillets();
				}
				else{$billets=null;}
				return $this->render('TrainCoreBundle:Default:admin_view.html.twig',
				array('voyage'=>$voyage, 'billets'=>$billets));
				}
				else{
							$authenticationUtils = $this->get('security.authentication_utils');
						return $this->render('TrainUserBundle:Default:login.html.twig', array(
						  'last_username' => $authenticationUtils->getLastUsername(),
						  'error'         => $authenticationUtils->getLastAuthenticationError(),
						));
		
				}
		
	}
		public function admin_createAction(Request $request){
				if($this->get('security.context')->isGranted('ROLE_ADMIN')){
				$em = $this->getDoctrine()->getManager();
				
				$voyage = new Voyage();
				$formBuilder = $this->get('form.factory')->createBuilder('form', $voyage); 
			    $formBuilder
				  ->add('date',      'datetime')
				  ->add('dateArrivee',      'datetime')
				  ->add('depart',     'text')
				  ->add('destination',   'text')
				  ->add('prixBambin',    'number')
				  ->add('prixEnfant','number')
				  ->add('prixJeune','number')
				  ->add('prixAdulte','number')
				  ->add('enregistrer','submit');
				  
					  $form = $formBuilder->getForm();
					  
					  
					  $form->handleRequest($request);
						
					if($form->isValid()){
						$em->persist($voyage);
						$em->flush();
						$this->get('session')->getFlashBag()->add('notice','Le voyage est bien enregistré.');						
					}

		
		
		return $this->render('TrainCoreBundle:Default:admin_create.html.twig',array('form'=>$form->createView()));		
		}
				
				
				else{
							$authenticationUtils = $this->get('security.authentication_utils');
						return $this->render('TrainUserBundle:Default:login.html.twig', array(
						  'last_username' => $authenticationUtils->getLastUsername(),
						  'error'         => $authenticationUtils->getLastAuthenticationError(),
						));
		
				}
		
	}

public function tosAction(Request $request){
	
	return $this->render('TrainCoreBundle:Default:terms_of_service.html.twig');
}	
public function cmcAction(Request $request){
	
	return $this->render('TrainCoreBundle:Default:comment_ca_marche.html.twig');
}

	
	}